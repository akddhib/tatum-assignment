locals {
  lambda_functions = {
    "put-data" = {
      source_file = "lambda-payloads/put-data/index.py"
    },
    "retrieve-data" = {
      source_file = "lambda-payloads/retrieve-data/index.py"
    }
  }
}
# zipit function
data "archive_file" "zipit" {
  for_each    = local.lambda_functions
  type        = "zip"
  source_file = "${path.module}/${each.value["source_file"]}"
  output_path = "${path.module}/${each.key}.zip"
}

# pull-data function
resource "aws_lambda_function" "put_openweathermap" {
  depends_on       = [data.archive_file.zipit]
  filename         = "${path.module}/put-data.zip"
  function_name    = "${local.prefix}-put-data"
  handler          = "index.lambda_handler"
  source_code_hash = data.archive_file.zipit["put-data"].output_base64sha256
  runtime          = "python3.9"
  role             = aws_iam_role.put_openweathermap.arn
  timeout          = 120

  environment {
    variables = {
      bucket = var.s3_bucket_name
      city   = var.city
    }
  }

  tags = local.common_tags
}

# Lambda pull_openweathermap IAM role and attached policies
resource "aws_iam_role" "put_openweathermap" {
  name = "${local.prefix}-put-openweathermap"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF

   tags = local.common_tags
}

resource "aws_iam_role_policy" "put_openweathermap" {
  name   = "${local.prefix}-put-openweathermap"
  role   = aws_iam_role.put_openweathermap.id
  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "logs:PutLogEvents",
                "logs:CreateLogGroup",
                "logs:CreateLogStream"
            ],
            "Resource": "arn:aws:logs:*:*:*"
        },
        {
            "Effect": "Allow",
            "Action": [
                "ssm:GetParameter"
            ],
            "Resource": "arn:aws:ssm:${local.region_id}:${local.acc_id}:parameter/openweather/apikey"
        },
        {
            "Effect": "Allow",
            "Action": [
                "s3:PutObject"
            ],
            "Resource": "arn:aws:s3:::openweathermap-storedata/*"
        }
    ]
}
EOF
}

# retrieve-data function
resource "aws_lambda_function" "retrieve_openweathermap" {
  depends_on       = [data.archive_file.zipit]
  filename         = "${path.module}/retrieve-data.zip"
  function_name    = "${local.prefix}-retrieve-data"
  handler          = "index.lambda_handler"
  source_code_hash = data.archive_file.zipit["retrieve-data"].output_base64sha256
  runtime          = "python3.9"
  role             = aws_iam_role.retrieve_openweathermap.arn
  timeout          = 120

  environment {
    variables = {
      bucket = var.s3_bucket_name
    }
  }

  tags = local.common_tags
}

# Lambda retrieve_openweathermap IAM role and attached policies
resource "aws_iam_role" "retrieve_openweathermap" {
  name = "${local.prefix}-retrieve-openweathermap"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF

  tags = local.common_tags
}

resource "aws_iam_role_policy" "retrieve_openweathermap" {
  name   = "${local.prefix}-retrieve-openweathermap"
  role   = aws_iam_role.retrieve_openweathermap.id
  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "logs:PutLogEvents",
                "logs:CreateLogGroup",
                "logs:CreateLogStream"
            ],
            "Resource": "arn:aws:logs:*:*:*"
        },
        {
            "Effect": "Allow",
            "Action": [
                "s3:GetObject"
            ],
            "Resource": "arn:aws:s3:::openweathermap-storedata/*"
        },
        {
            "Effect": "Allow",
            "Action": [
                "s3:ListBucket"
            ],
            "Resource": [
                "arn:aws:s3:::openweathermap-storedata"
            ]
        }
    ]
}
EOF
}