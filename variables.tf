variable "s3_bucket_name" {
  description = "s3 bucket where we store openweathermap CSV data"
  default     = "openweathermap-storedata"
}

variable "city" {
  description = "city data we want to retrieve from openweathermap API"
  default     = "Paris"
}

variable "lambda_put_openweathermap" {
  description = "put-data lambda execution cronjob rate"
  default     = "rate(1 hour)"
}