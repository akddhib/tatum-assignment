output "deployment_invoke_url" {
  description = "retrieve state information endpoint"
  value       = "${aws_api_gateway_stage.retrieve_openweathermap.invoke_url}/${trimprefix("${aws_api_gateway_resource.retrieve_openweathermap.path}", "/")}"
}