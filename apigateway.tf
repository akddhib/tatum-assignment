resource "aws_api_gateway_rest_api" "retrieve_openweathermap" {
  name = "${local.prefix}-retrieve-openweathermap"
  endpoint_configuration {
    types = ["REGIONAL"]
  }
  tags = local.common_tags
}

resource "aws_lambda_permission" "retrieve_openweathermap" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.retrieve_openweathermap.function_name
  principal     = "apigateway.amazonaws.com"

  source_arn = "${aws_api_gateway_rest_api.retrieve_openweathermap.execution_arn}/*/*"
}

resource "aws_api_gateway_resource" "retrieve_openweathermap" {
  parent_id   = aws_api_gateway_rest_api.retrieve_openweathermap.root_resource_id
  path_part   = "getstate"
  rest_api_id = aws_api_gateway_rest_api.retrieve_openweathermap.id
}

# GET method integration

resource "aws_api_gateway_request_validator" "retrieve_openweathermap" {
  name                        = "${local.prefix}-retrieve-openweathermap"
  rest_api_id                 = aws_api_gateway_rest_api.retrieve_openweathermap.id
  validate_request_body       = false
  validate_request_parameters = true
}

resource "aws_api_gateway_method" "retrieve_openweathermap" {
  authorization        = "NONE"
  http_method          = "GET"
  resource_id          = aws_api_gateway_resource.retrieve_openweathermap.id
  rest_api_id          = aws_api_gateway_rest_api.retrieve_openweathermap.id
  request_validator_id = aws_api_gateway_request_validator.retrieve_openweathermap.id

  request_parameters = {
    "method.request.querystring.state" = true
  }
}

resource "aws_api_gateway_integration" "retrieve_openweathermap" {
  rest_api_id             = aws_api_gateway_rest_api.retrieve_openweathermap.id
  resource_id             = aws_api_gateway_resource.retrieve_openweathermap.id
  http_method             = aws_api_gateway_method.retrieve_openweathermap.http_method
  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = aws_lambda_function.retrieve_openweathermap.invoke_arn
}

resource "aws_api_gateway_method_response" "retrieve_openweathermap" {
  rest_api_id = aws_api_gateway_rest_api.retrieve_openweathermap.id
  resource_id = aws_api_gateway_resource.retrieve_openweathermap.id
  http_method = aws_api_gateway_method.retrieve_openweathermap.http_method
  status_code = "200"
  response_parameters = {
    "method.response.header.Content-Type" = true
  }
}

# Api Gateway deployment
resource "aws_api_gateway_deployment" "retrieve_openweathermap" {
  depends_on = [
    aws_api_gateway_integration.retrieve_openweathermap
  ]
  triggers = {
    redeployment = sha1(jsonencode([
      aws_api_gateway_resource.retrieve_openweathermap.id,
      aws_api_gateway_method.retrieve_openweathermap.id,
      aws_api_gateway_integration.retrieve_openweathermap.id,
    ]))
  }

  lifecycle {
    create_before_destroy = true
  }
  rest_api_id = aws_api_gateway_rest_api.retrieve_openweathermap.id
}

resource "aws_api_gateway_stage" "retrieve_openweathermap" {
  deployment_id = aws_api_gateway_deployment.retrieve_openweathermap.id
  rest_api_id   = aws_api_gateway_rest_api.retrieve_openweathermap.id
  stage_name    = "${local.prefix}"

  tags_all = local.common_tags
}