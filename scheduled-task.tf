# CloudWatch scheduled task
resource "aws_cloudwatch_event_rule" "put_openweathermap" {
  name = "${local.prefix}-put-openweathermap"
  schedule_expression = var.lambda_put_openweathermap

  tags = local.common_tags
}

resource "aws_cloudwatch_event_target" "put_openweathermap" {
  rule = aws_cloudwatch_event_rule.put_openweathermap.name
  arn  = aws_lambda_function.put_openweathermap.arn
}

resource "aws_lambda_permission" "put_openweathermap" {
  statement_id  = "AllowExecutionFromCloudWatch"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.put_openweathermap.function_name
  principal     = "events.amazonaws.com"
  source_arn    = aws_cloudwatch_event_rule.put_openweathermap.arn
}