# tatum-assignment
1. Download regularly (default hourly rate as per `lambda_put_openweathermap` value in `variables.tf`) from a free data provider (e.g http://api.openweathermap.org/geo/1.0/direct?q=London&limit=5&appid={API key})
2. Store downloaded dataset to s3 bucket as CSV files
3. Extract some specific state data (e.g. data relevant to `England`, `Ile-de-France`..)
4. Display all extracted data using a single HTML page served from S3 bucket. A simple table is enough.

# Prerequisites
#### Install AWS CLI
1. [Install AWS CLI](http://docs.aws.amazon.com/cli/latest/userguide/installing.html)
2. Get AWS CLI credentials
3. Go to [AWS Console: IAM](https://console.aws.amazon.com/iam/home)
4. Create a user that have `AdministratorAccess` IAM permission with access types `Access key - Programmatic access` and `Password - AWS Management Console access`
4. Select **Security Credentials**
5. Select **Access Key ID** and **Secret access key**
6. Configure AWS CLI, run `aws configure` using above parameters.

#### Create AWS resources from AWS console
1. an S3 bucket to host terraform state named `fs-tatum-app-state`
2. A DynamoDB table to handle state locking named `fs-tatum-state` and should have primary key `LOCK_ID`
3. an S3 bucket to host downloaded data from API endpoint named `openweathermap-storedata` with KMS encryption and public access blocked.

#### Create API key from openweathermap.org
1. Create an API key under https://home.openweathermap.org/api_keys to access openweathermap 
2. store its value as encrypted String to SSM parameter store under path `/openweather/apikey`

#### Install Docker
You will need to install [Docker](https://docs.docker.com/engine/installation/) and [Docker Compose](https://docs.docker.com/compose/install/) to get the app running locally in your machine.

#### Setup Gitlab user
1. Go to [AWS Console: IAM](https://console.aws.amazon.com/iamv2/home#/users)
2. Select **Add users** with only access type `Access key - Programmatic access`
3. Select **Next Permissions**
4. Select **Create policy** and give the user the custom IAM permission inside `gitlab-policy/policy.json`
5. Select **Access Key ID** and  **Secret access key**
6. Create two Gitlab protected variables `AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY` using above parameters.

# Technical implementation
1. Build a scheduled Lambda function `put-data` that will use this GeoCoding API http://api.openweathermap.org/geo/1.0/direct?q=London&limit=5&appid={API key} to collect a given city data and store it to an S3 bucket as CSV files (default city value is `Paris` under `variables.tf`)
2. Build an Api Gateway / Lambda `retrieve-data` integration that fetch those CSVs for a given state information and display it back as a single HTML page.

# CICD implementation
We have one main gitlab pipeline that is triggered based on below scenarios:
-  If merge request issued to `main`, a `Validate Terraform` job is triggered.
-  If merged/pushed to `main`, both `Validate Terraform` and `production` deployment will be rolled out.

# Try the Stack on Gitlab
1. deploy to production
2. wait for scheduled lambda job until it stores CSV datas in `openweathermap-storedata` bucket
2. get `deployment_invoke_url` from terraform output (e.g https://XXX.eu-central-1.amazonaws.com/production/getstate)
3. Display a given state from stored data as follow:  `https://XXX.eu-central-1.amazonaws.com/production/getstate?state=xxx`

# Local implementation
You can access and deploy to $ENV (production or development) as follow:

1. chose the environment you want to destroy:
   - docker-compose run --rm terraform workspace select $ENV
2. Plan the whole stack:
   - docker-compose run --rm terraform plan
3. Apply the whole stack:
   - docker-compose run --rm terraform apply -auto-approve
4. clean the stack:
   - docker-compose run --rm terraform destroy -auto-approve
   - docker-compose run --rm terraform workspace select default
   - docker-compose run --rm terraform workspace delete $ENV

## Encountered problems
1. `terraform fmt -check` is raising an error with `Validate Terraform` job so I commented it.

## TODO
2. Setup custom domain name with the API Gateway.