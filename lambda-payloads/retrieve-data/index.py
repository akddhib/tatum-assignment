import boto3
import json
import os
client = boto3.client('s3')
S3_BUCKET = os.environ['bucket']

def get_s3_keys(S3_BUCKET):
    S3_keys = []
    paginator = client.get_paginator('list_objects_v2')
    result = paginator.paginate(Bucket=S3_BUCKET)
    for page in result:
        if "Contents" in page:
            for key in page["Contents"]:
                keyString = key["Key"]
                S3_keys.append(keyString)
        return S3_keys


def lambda_handler(event, context):
    formatted_records = []
    keys = get_s3_keys(S3_BUCKET)
    print(event)
    state = event["queryStringParameters"]["state"]
    expression = "Select * from s3object s where s.state like '"+state+"%'"
    print(expression)
    for key in keys:
        response = client.select_object_content(
            Bucket=S3_BUCKET,
            Key=key,
            ExpressionType='SQL',
            Expression=expression,
            InputSerialization={'CSV': {"FileHeaderInfo": "Use"}},
            OutputSerialization={'CSV': {}}
        )
        # print(reponse)
        for event in response['Payload']:
            if 'Records' in event:
                records = event['Records']['Payload'].decode('utf-8')
                # print(records)
                formatted_recordslist = records.split("\n")
                while('' in formatted_recordslist):
                    formatted_recordslist.remove('')
    html = '<!DOCTYPE html><html><head><title>HTML from HTTP API Gateway/Lambda</title></head><body>'
    html += '<table><tr><td>City</td><td>longitute</td><td>latitude</td><td>state</td></tr>'
    for rec in formatted_recordslist:
        html += '<tr>'
        for val in rec.split(","):
            html += '<td>' + val + '</td>'
        html += '</tr>'
    html += '</table></body></html>'

    return {
        "statusCode": 200,
        "body": html,
        "headers": {
        'Content-Type': 'text/html',
        }
    }