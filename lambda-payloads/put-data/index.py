import boto3
import csv
from datetime import datetime, timezone
import json
from os import listdir
from os.path import isfile, join
import urllib3
import os

s3_client = boto3.client("s3")
ssm_client = boto3.client('ssm')

LOCAL_FILE_SYS = "/tmp"
S3_BUCKET = os.environ['bucket']
city = os.environ['city']
apikey = ssm_client.get_parameter(Name='/openweather/apikey', WithDecryption=True)

def _get_key():
    dt_now = datetime.now(tz=timezone.utc)
    KEY = (
        dt_now.strftime("%Y-%m-%d")
        + "/"
        + dt_now.strftime("%H")
        + "/"
        + dt_now.strftime("%M")
        + "/"
    )
    return KEY


def get_data(
    country, appid, limit="5", get_path="http://api.openweathermap.org/geo/1.0/direct"
):
    http = urllib3.PoolManager()
    data = {"name": None, "lat": None,
            "lon": None, "state": None, "body": None}
    try:
        r = http.request(
            "GET",
            get_path,
            retries=urllib3.util.Retry(3),
            fields={"q": country, "limit": limit, "appid": appid},
        )
        data = json.loads(r.data.decode("utf8").replace("'", '"'))
    except KeyError as e:
        print(f"Wrong format url {get_path}", e)
    except urllib3.exceptions.MaxRetryError as e:
        print(f"API unavailable at {get_path}", e)
    return data

def write_to_local(data, loc=LOCAL_FILE_SYS):
    file_name = loc + "/" + "states.csv"
    with open(file_name, "w") as file:
        writer = csv.writer(file)
        writer.writerow(['name','lat','lon','state'])
        for elt in data:
            writer.writerow([elt.get("name"),elt.get("lat"),elt.get("lon"),elt.get("state")])
    file.close()
    return file_name


def save_data(city):
    data = get_data(city, apikey['Parameter']['Value'])
    write_to_local(data)
    return data


def lambda_handler(event, context):
    data = save_data(city)
    key = _get_key()
    files = [f for f in listdir(LOCAL_FILE_SYS)
             if isfile(join(LOCAL_FILE_SYS, f))]
    for f in files:
        s3_client.upload_file(LOCAL_FILE_SYS + "/" + f, S3_BUCKET, key + f)
